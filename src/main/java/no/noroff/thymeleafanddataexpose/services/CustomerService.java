package no.noroff.thymeleafanddataexpose.services;

import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;

import java.util.List;

public interface CustomerService {
    List<Customer> getAll();
    List<Customer> getAllCustomersWithNameLike();
    Customer getByCustomerId();
    List<CountryCount> getCustomersGroupbyCountry();
    List<CustomerSpender> getTotalSpendings();
    List<CustomerGenre> getTopSellingGenreById();

    List<Customer> getCustomersBetweenIds();

    int add();
    int update();
    int delete();
}
