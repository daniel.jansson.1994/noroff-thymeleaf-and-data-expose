package no.noroff.thymeleafanddataexpose.services;

import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;
import no.noroff.thymeleafanddataexpose.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> getAll() {
        return customerRepository.getAll();
    }

    @Override
    public List<CustomerGenre> getTopSellingGenreById() {
        return customerRepository.getTopSellingGenreById();
    }

    @Override
    public List<CountryCount> getCustomersGroupbyCountry() {
        return customerRepository.getCustomersGroupbyCountry();
    }

    @Override
    public List<Customer> getAllCustomersWithNameLike() {
        return customerRepository.getAllCustomersWithNameLike();
    }

    @Override
    public Customer getByCustomerId() {
        return customerRepository.getByCustomerId();
    }

    @Override
    public List<CustomerSpender> getTotalSpendings() {
        return customerRepository.getTotalSpendings();
    }

    @Override
    public List<Customer> getCustomersBetweenIds() {
        return customerRepository.getCustomersBetweenIds();
    }

    @Override
    public int add() {
        return 0;
    }

    @Override
    public int update() {
        return 0;
    }

    @Override
    public int delete() {
        return 0;
    }
}
