package no.noroff.thymeleafanddataexpose.controllers;

import no.noroff.thymeleafanddataexpose.dao.MusicDAO;
import no.noroff.thymeleafanddataexpose.models.SongSearch;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping()
public class MusicController {

    // Renders home page view with five random artists, songs and genres, and ties search to songSearch object
    @GetMapping()
    public String home(Model model) {
        model.addAttribute("artists", MusicDAO.getFiveArtists());
        model.addAttribute("songs", MusicDAO.getFiveSongs());
        model.addAttribute("genres", MusicDAO.getFiveGenres());
        model.addAttribute("songSearch", new SongSearch());
        return "view-home-page";
    }

    // Renders search result view with songSearch object
    @PostMapping()
    public String submitSearch(@ModelAttribute SongSearch songSearch, BindingResult errors, Model model) {
        // To make the search case-insensitive
        String searchToLowerCase = songSearch.getName().toLowerCase();

        // Checks the postmapping for errors, goes through with the song search otherwise
        if (errors.hasErrors()) {
            System.out.println("ERROR");
        } else {
            model.addAttribute("songResult", MusicDAO.getSongByName(searchToLowerCase));
        }
        return "view-search-result";
    }
}
