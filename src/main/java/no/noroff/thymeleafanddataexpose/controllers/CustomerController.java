package no.noroff.thymeleafanddataexpose.controllers;

import no.noroff.thymeleafanddataexpose.dao.CustomerDAO;
import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {
    CustomerDAO customerDAO = new CustomerDAO();

    public CustomerController() {
    }

    @GetMapping
    public List<Customer> getAll() {
        return customerDAO.getAllCustomers();
    }

    @GetMapping("spending")
    public List<CustomerSpender> getTotalSpendings() {
        return customerDAO.getTotalSpendings();
    }

    @GetMapping("nameLike/search")
    public List<Customer> getAllCustomersWithNameLike(@RequestParam(value="firstname") String firstName, @RequestParam(value="lastname") String lastName) {
        return customerDAO.getAllCustomersWithNameLike(firstName + "%", lastName + "%");
    }

    @GetMapping("{id}")
    public Customer getCustomerById(@PathVariable String id) {
        return customerDAO.getCustomerById(id);
    }

    @GetMapping("betweenIds/search")
    public List<Customer> getCustomersBetweenIds(@RequestParam(value="idStart") String id1, @RequestParam(value="idEnd") String id2) {
        return customerDAO.getCustomersBetweenIds(id1, id2);
    }

    @GetMapping("groupedby/countries")
    public List<CountryCount> getCustomersGroupbyCountry() {
        return customerDAO.getCustomersGroupbyCountry();
    }

    @PostMapping()
    public int add(@RequestBody Customer customer) {
        return customerDAO.addCustomer(customer);
    }

    @PatchMapping()
    public int update(@RequestBody Customer customer) {
        return customerDAO.updateCustomer(customer);
    }

    @GetMapping({"genre/{id}"})
    public List<CustomerGenre> getTopSellingGenreById(@PathVariable String id) {
        return customerDAO.getTopSellingGenreById(id);
    }
}
