package no.noroff.thymeleafanddataexpose.dao;

import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {
    private final String connectionString = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    /**
     * Will get all customers in the database.
     */
    public List<Customer> getAllCustomers() {

        String sql = "SELECT customerid, firstname, lastname, city, country, postalCode, phone, email FROM Customer";
        //List of customers for the data
        List<Customer> returnCustomers = new ArrayList<>();
        //Database connection
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Executing query from sql string
            ResultSet resultSet = prepStatement.executeQuery();
            //Add a new customer in the list, until there is no next customer.
            while (resultSet.next()) {
                returnCustomers.add(
                        new Customer(
                                resultSet.getString("customerid"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("City"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomers;
    }

    /**
     * Will get all the customers with a name equal to some letters and %
     */
    public List<Customer> getAllCustomersWithNameLike(String firstName, String lastName) {
        String sql = "SELECT customerid, firstname, lastname, city, country, postalcode, phone, email FROM Customer WHERE firstname LIKE ? AND lastname LIKE ?";
        //List for the data
        List<Customer> returnsCustomers = new ArrayList<>();
        //Database connection
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            //Executing the sql query
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Setting the parameters for the SQL-query.
            prepStatement.setString(1, firstName);
            prepStatement.setString(2, lastName);
            ResultSet resultSet = prepStatement.executeQuery();
            //Add a new customer in the list, until there is no next customer.
            while (resultSet.next()) {
                returnsCustomers.add(
                        new Customer(
                                resultSet.getString("customerid"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("City"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return returnsCustomers;
    }

    /**
     * Will get a customer by id
     */
    public Customer getCustomerById(String id) {
        Customer returnCustomer = null;

        String sql = "SELECT customerid, firstname, lastname, city, country, postalcode, phone, email FROM Customer WHERE customerid = ?";
        //Connecting to the database
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            //Setting the SQL-query which will be used.
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Setting the parameters for the query
            prepStatement.setString(1, id);
            //Executing the query
            ResultSet resultSet = prepStatement.executeQuery();
            //Add the new customer.
            returnCustomer = new Customer(
                    resultSet.getString("customerid"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("City"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomer;
    }

    /**
     * Will get all customers between two selected ids
     */
    public List<Customer> getCustomersBetweenIds(String id1, String id2) {

        String sql = "SELECT customerid, firstname, lastname,city,  country, postalcode, phone, email FROM Customer WHERE customerid BETWEEN ? AND ?";
        //Sets up the list for later data storing
        List<Customer> returnsCustomers = new ArrayList<>();
        //Connection to the database
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            //
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Setting the parameters for the query
            prepStatement.setString(1, id1);
            prepStatement.setString(2, id2);
            //Executing the SQL query
            ResultSet resultSet = prepStatement.executeQuery();
            //Add a new customer in the list, until there is no next customer.
            while (resultSet.next()) {
                returnsCustomers.add(
                        new Customer(
                                resultSet.getString("customerid"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("City"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnsCustomers;
    }

    /**
     * Will get all customers counted by nationality and displayed by a count of each nationality
     */
    public List<CountryCount> getCustomersGroupbyCountry() {
        String sql = "SELECT country, COUNT(*) AS test from Customer group BY country ORDER BY test DESC";
        //List for later data storing
        List<CountryCount> returnsCustomers = new ArrayList<>();
        //Connecting to database
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Executing the sql query
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                returnsCustomers.add(
                        new CountryCount(
                                resultSet.getString("Country"),
                                resultSet.getString("test")
                        ));
            }
            //Throw expection if database connection fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnsCustomers;
    }

    /**
     * Adds a customer
     */
    public int addCustomer(Customer customer) {
        int result = 0;
        String sql = "INSERT INTO Customer (customerid, firstname, lastname, country, postalcode, phone, email) VALUES(?, ?, ?, ?, ?, ?, ?)";
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStmt = conn.prepareStatement(sql);
            prepStmt.setString(1, customer.CustomerId());
            prepStmt.setString(2, customer.FirstName());
            prepStmt.setString(3, customer.LastName());
            prepStmt.setString(4, customer.Country());
            prepStmt.setString(5, customer.PostalCode());
            prepStmt.setString(6, customer.Phone());
            prepStmt.setString(7, customer.Email());
            //Executing the sql query
            result = prepStmt.executeUpdate();
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Updates the values a user has stored in the database.
     */
    public int updateCustomer(Customer customer) {
        int result = 0;
        String sql = "UPDATE Customer SET firstname = ?, lastname = ?, country = ?, postalcode = ?, phone = ?, email = ? WHERE customerid = ?";
        //Database connection
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStmt = conn.prepareStatement(sql);
            prepStmt.setString(1, customer.FirstName());
            prepStmt.setString(2, customer.LastName());
            prepStmt.setString(3, customer.Country());
            prepStmt.setString(4, customer.PostalCode());
            prepStmt.setString(5, customer.Phone());
            prepStmt.setString(6, customer.Email());
            prepStmt.setString(7, customer.CustomerId());
            //Executing the update
            result = prepStmt.executeUpdate();
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Will get the total sum of purchases from each customer
     */
    public List<CustomerSpender> getTotalSpendings() {
        String sql = "SELECT c.CustomerId, c.firstname, c.lastname, c.phone, c.email, c.country, SUM(i.total) as Total_payment FROM Customer as c, Invoice as i WHERE c.CustomerId = i.customerid GROUP by c.customerid ORDER BY Total_payment desc";
        //List to store the data from the query
        List<CustomerSpender> returnCustomers = new ArrayList<>();
        //Connection to the database
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Executing the sql query
            ResultSet resultSet = prepStatement.executeQuery();
            while (resultSet.next()) {
                returnCustomers.add(
                        new CustomerSpender(
                                resultSet.getString("customerid"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getString("phone"),
                                resultSet.getString("email"),
                                resultSet.getString("country"),
                                resultSet.getString("Total_payment")
                        ));
            }
            //Throws exception if connection to database fails
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomers;
    }

    /**
     * Getting top purchased genres for a customer
     */
    public List<CustomerGenre> getTopSellingGenreById(String id) {
        //Added as list in case of a tie
        List<CustomerGenre> returnCustomer = new ArrayList<>();
        String sql = "WITH tracks1 AS (" +
                "SELECT " +
                "COUNT(i.InvoiceId) TotalPurchase, g.Name AS GenreName, g.GenreId, c.CustomerId, c.firstname, c.LastName " +
                "FROM Invoice AS i " +
                "JOIN Customer c ON i.CustomerId = c.CustomerId " +
                "JOIN InvoiceLine il ON il.Invoiceid = i.Invoiceid " +
                "JOIN Track t ON t.TrackId = il.Trackid " +
                "JOIN Genre g ON t.GenreId = g.GenreId " +
                "WHERE c.customerid = ? " +
                "GROUP BY GenreName " +
                ") " +
                "SELECT tracks1.* " +
                "FROM tracks1 " +
                "JOIN ( " +
                "SELECT MAX(TotalPurchase) AS TotalPurchases, GenreId, customerid " +
                "FROM tracks1 " +
                ")tracks2 " +
                "ON tracks1.customerid = tracks2.customerid " +
                "WHERE tracks1.TotalPurchase = tracks2.TotalPurchases";
        //Connection to the database
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            //Setting the id parameter for the sql query
            prepStatement.setString(1, id);
            //Executing the sql query
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                returnCustomer.add(
                        new CustomerGenre(
                                resultSet.getString("TotalPurchase"),
                                resultSet.getString("GenreName"),
                                resultSet.getString("GenreId"),
                                resultSet.getString("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName")
                        ));
            }
            //Throws exception if connection fails.
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnCustomer;
    }
}
