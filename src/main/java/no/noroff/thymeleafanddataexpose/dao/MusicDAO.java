package no.noroff.thymeleafanddataexpose.dao;

import no.noroff.thymeleafanddataexpose.models.Artist;
import no.noroff.thymeleafanddataexpose.models.Genre;
import no.noroff.thymeleafanddataexpose.models.Song;
import no.noroff.thymeleafanddataexpose.models.SongSearch;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MusicDAO {
    private static final String connectionString = "jdbc:sqlite::resource:Chinook_Sqlite.sqlite";

    /**
     * Get five random artists
     */
    public static List<Artist> getFiveArtists() {
        String sql = "SELECT artistid, name FROM Artist ORDER BY Random() LIMIT 5";
        List<Artist> returnArtists = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            ResultSet resultSet = prepStatement.executeQuery();
            while (resultSet.next()) {
                returnArtists.add(
                        new Artist(resultSet.getInt("ArtistId"),
                                resultSet.getString("Name")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnArtists;
    }

    /**
     * Get five random songs
     */
    public static List<Song> getFiveSongs() {
        String sql = "SELECT trackid, name, albumid, genreid, composer FROM Track ORDER BY Random() LIMIT 5;\n";
        List<Song> returnSongs = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                returnSongs.add(
                        new Song(resultSet.getInt("TrackId"),
                                resultSet.getString("Name"),
                                resultSet.getInt("AlbumId"),
                                resultSet.getInt("GenreId"),
                                resultSet.getString("Composer")
                        ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnSongs;
    }

    /**
     * Get five random genre
     */
    public static List<Genre> getFiveGenres() {
        String sql = "SELECT genreid, name FROM Genre ORDER BY Random() LIMIT 5";
        List<Genre> returnGenres = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement prepStatement = conn.prepareStatement(sql);
            ResultSet resultSet = prepStatement.executeQuery();

            while (resultSet.next()) {
                returnGenres.add(new Genre(
                        resultSet.getInt("GenreId"),
                        resultSet.getString("Name")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnGenres;
    }

    /**
     * Gets a song, its artist, the album title and genre by the song name
     */
    public static SongSearch getSongByName(String name) {
        // Sql query
        String sql =
                "SELECT t.name, ar.name as artist, al.title as album, g.name as genre FROM Track AS t " +
                        "JOIN Artist AS ar ON ar.ArtistId = al.ArtistId " +
                        "JOIN Album AS al ON al.AlbumId = t.AlbumId " +
                        "JOIN Genre AS g ON g.GenreId = t.GenreId " +
                        "WHERE lower(t.name) = ?";
        SongSearch returnSong = null;

        // Tries to execute and adds name, artist, album and genre to song object
        try (Connection conn = DriverManager.getConnection(connectionString)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                returnSong = new SongSearch(
                        resultSet.getString("name"),
                        resultSet.getString("artist"),
                        resultSet.getString("album"),
                        resultSet.getString("genre")
                );
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return returnSong;
    }
}