package no.noroff.thymeleafanddataexpose;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeLeafAndDataExposeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymeLeafAndDataExposeApplication.class, args);
    }

}
