package no.noroff.thymeleafanddataexpose.repositories;

import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository{
    @Override
    public List<Customer> getAll(){ return null; }

    @Override
    public List<Customer> getAllCustomersWithNameLike(){ return null; }

    @Override
    public Customer getByCustomerId() { return null; }

    @Override
    public List<Customer> getCustomersBetweenIds() { return null; }

    @Override
    public List<CountryCount> getCustomersGroupbyCountry() { return null; }

    @Override
    public List<CustomerSpender> getTotalSpendings() {return null;}

    @Override
    public List<CustomerGenre> getTopSellingGenreById() {return null; }

    @Override
    public int add() {
        return 0;
    }

    @Override
    public int update() {
        return 0;
    }

    @Override
    public int delete() {
        return 0;
    }
}
