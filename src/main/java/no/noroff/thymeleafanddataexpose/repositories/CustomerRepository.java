package no.noroff.thymeleafanddataexpose.repositories;

import no.noroff.thymeleafanddataexpose.models.CountryCount;
import no.noroff.thymeleafanddataexpose.models.Customer;
import no.noroff.thymeleafanddataexpose.models.CustomerGenre;
import no.noroff.thymeleafanddataexpose.models.CustomerSpender;

import java.util.List;

public interface CustomerRepository {
    List<Customer> getAll();
    List<Customer> getAllCustomersWithNameLike();
    List<Customer> getCustomersBetweenIds();
    List<CountryCount> getCustomersGroupbyCountry();
    List<CustomerSpender> getTotalSpendings();
    List<CustomerGenre> getTopSellingGenreById();
    Customer getByCustomerId();
    int add();
    int update();
    int delete();
}
