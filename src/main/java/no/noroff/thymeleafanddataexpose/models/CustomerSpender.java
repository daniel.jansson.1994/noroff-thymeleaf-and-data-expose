package no.noroff.thymeleafanddataexpose.models;

public record CustomerSpender(String CustomerId, String FirstName, String LastName, String Phone, String Email, String Country, String TotalPayment) {
}
