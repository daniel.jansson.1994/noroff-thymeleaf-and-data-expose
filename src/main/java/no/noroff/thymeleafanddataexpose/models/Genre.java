package no.noroff.thymeleafanddataexpose.models;

import java.util.Objects;

public class Genre {
    private int GenreId;
    private String Name;

    public Genre() {
    }

    public Genre(int genreId, String name) {
        GenreId = genreId;
        Name = name;
    }

    public int getGenreId() {
        return GenreId;
    }

    public void setGenreId(int genreId) {
        GenreId = genreId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return GenreId == genre.GenreId && Objects.equals(Name, genre.Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(GenreId, Name);
    }

    @Override
    public String toString() {
        return "Genre{" +
                "GenreId=" + GenreId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
