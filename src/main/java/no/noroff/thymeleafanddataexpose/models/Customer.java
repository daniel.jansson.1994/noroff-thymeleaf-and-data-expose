package no.noroff.thymeleafanddataexpose.models;

public record Customer (String CustomerId, String FirstName, String LastName, String City, String Country, String PostalCode, String Phone, String Email) {
}
