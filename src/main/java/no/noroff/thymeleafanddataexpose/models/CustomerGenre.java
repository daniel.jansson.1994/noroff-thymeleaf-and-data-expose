package no.noroff.thymeleafanddataexpose.models;

public record CustomerGenre(String TotalPurchase, String GenreName, String GenreId, String CustomerId, String FirstName, String LastName) {
}
