package no.noroff.thymeleafanddataexpose.models;

import java.util.Objects;

public class Song {
    private int TrackId;
    private String Name;
    private int AlbumId;
    private int GenreId;
    private String Composer;

    public Song() {
    }

    public Song(int trackId, String name, int albumId, int genreId, String composer) {
        TrackId = trackId;
        Name = name;
        AlbumId = albumId;
        GenreId = genreId;
        Composer = composer;
    }

    public int getTrackId() {
        return TrackId;
    }

    public void setTrackId(int trackId) {
        TrackId = trackId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(int albumId) {
        AlbumId = albumId;
    }

    public int getGenreId() {
        return GenreId;
    }

    public void setGenreId(int genreId) {
        GenreId = genreId;
    }

    public String getComposer() {
        return Composer;
    }

    public void setComposer(String composer) {
        Composer = composer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return TrackId == song.TrackId && AlbumId == song.AlbumId && GenreId == song.GenreId && Objects.equals(Name, song.Name) && Objects.equals(Composer, song.Composer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(TrackId, Name, AlbumId, GenreId, Composer);
    }

    @Override
    public String toString() {
        return "Song{" +
                "TrackId=" + TrackId +
                ", Name='" + Name + '\'' +
                ", AlbumId=" + AlbumId +
                ", GenreId=" + GenreId +
                ", Composer='" + Composer + '\'' +
                '}';
    }
}
