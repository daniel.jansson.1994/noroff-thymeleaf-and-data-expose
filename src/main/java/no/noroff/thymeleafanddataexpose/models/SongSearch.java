package no.noroff.thymeleafanddataexpose.models;

import java.util.Objects;

public class SongSearch {
    private String name;
    private String artistName;
    private String albumTitle;
    private String genre;

    public SongSearch() {
    }

    public SongSearch(String name, String artistName, String albumTitle, String genre) {
        this.name = name;
        this.artistName = artistName;
        this.albumTitle = albumTitle;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public void setAlbumTitle(String albumTitle) {
        this.albumTitle = albumTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongSearch that = (SongSearch) o;
        return Objects.equals(name, that.name) && Objects.equals(artistName, that.artistName) && Objects.equals(albumTitle, that.albumTitle) && Objects.equals(genre, that.genre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, artistName, albumTitle, genre);
    }

    @Override
    public String toString() {
        return "SongSearch{" +
                "name='" + name + '\'' +
                ", artistName='" + artistName + '\'' +
                ", albumTitle='" + albumTitle + '\'' +
                ", genre='" + genre + '\'' +
                '}';
    }
}
