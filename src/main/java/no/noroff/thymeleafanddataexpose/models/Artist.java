package no.noroff.thymeleafanddataexpose.models;

import java.util.Objects;

public class Artist {
    private int ArtistId;
    private String Name;

    public Artist() {
    }

    public Artist(int artistId, String name) {
        ArtistId = artistId;
        Name = name;
    }

    public int getArtistId() {
        return ArtistId;
    }

    public void setArtistId(int artistId) {
        ArtistId = artistId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artist artist = (Artist) o;
        return ArtistId == artist.ArtistId && Objects.equals(Name, artist.Name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ArtistId, Name);
    }

    @Override
    public String toString() {
        return "Artist{" +
                "ArtistId=" + ArtistId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
