# Noroff assignment: Access and expose a database.

Spring Boot Application in Java. Thymeleaf for frontend.

Accessed and exposed the Chinook SQL Lite database through JDBC.

Application was published as a Docker container on Heroku.

## Table of contents
1. [Technologies/Dependencies](#technologies/dependencies)
2. [Usage](#usage)
3. [Link to view frontend through heroku](#link to view frontend through heroku)
4. [Link to view backend through heroku](#link to view backend through heroku)
5. [Authors](#authors)

## Technologies/Dependencies:
- Java 17
- Spring
- Sqlite
- Docker
- Thymeleaf

## Installation/Usage
- Install JDK 17
- Clone repository
- Build Gradle Jar
- Run application through the main class and method

## Link to view frontend through heroku:

- [Homepage](https://noroff-thymeleaf-dataexpose.herokuapp.com/)

## Link to view backend through heroku:

- [All customers](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/)
- [Highest spenders](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/spending)
- [Name like](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/nameLike/search?firstname=b&lastname=h)
- [Customer by id](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/5)
- [Customers between two ids](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/betweenIds/search?idStart=10&idEnd=20)
- [Customers based on nationality](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/groupedby/countries)
- [Most purchased genre for a id](https://noroff-thymeleaf-dataexpose.herokuapp.com/api/v1/customers/genre/5)

## Authors:
- Daniel Jansson
- Jo Endre Brauten Urdal